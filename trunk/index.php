<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Online Movie Ticketing System</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">SecureZ</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="about.html">About us</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products & Services <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="securez full security (multi-device).html">SecureZ Full Security (Multi-Device)</a>
                            </li>
                            <li>
                                <a href="securez internet security (multi-device).html">SecureZ Internet Security (Multi-Device)</a>
                            </li>
                            <li>
                                <a href="securez internet security.html">SecureZ Internet Security</a>
                            </li>
                            <li>
                                <a href="securez anti virus.html">SecureZ Anti Virus</a>
                            </li>
                            <li>
                                <a href="securez cleanup.html">SecureZ Cleanup</a>
                            </li>
                            <li>
                                <a href="securez virus scanner for mac.html">SecureZ Virus Scanner for Mac</a>
                            </li>
                            <li>
                                <a href="securez password manager.html">SecureZ Password Manager</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="login.php">Login</a>
                    </li>
                    <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Enter search keyword here">
                    </div>
                    <a type="submit" class="form-control-button btn btn-primary">Search</a>
                  </form>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	

    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('img/secure1.jpg');"></div>
                <div class="carousel-caption">
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/secure2.jpg');"></div>
                <div class="carousel-caption">
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('img/secure3.jpg');"></div>
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
		
        <div class="row">
            
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-home"></i> Security for Home</h4>
                    </div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?</p>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-building"></i> Security for Business</h4>
                    </div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?</p>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4> News and Resources</h4>
                    </div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, optio corporis quae nulla aspernatur in alias at numquam rerum ea excepturi expedita tenetur assumenda voluptatibus eveniet incidunt dicta nostrum quod?</p>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Portfolio Heading</h2>
            </div>
            <div class="col-md-4 col-sm-6">
				<h4 style="text-align: center">SecureZ Full Security (Multi-Device)</h4>
				</br>
                <a href="securez full security (multi-device).html">
                    <img style="margin:auto" class="img-responsive img-portfolio img-hover" src="img/1.png" alt="">
                </a>
				</br>
				</br>
            </div>
            <div class="col-md-4 col-sm-6">
                <h4 style="text-align: center">SecureZ Internet Security (Multi-Device)</h4>
				</br>
				<a href="securez internet security (multi-device).html">
                    <img style="margin:auto" class="img-responsive img-portfolio img-hover" src="img/4.png" alt="">
                </a>
				</br>
				</br>
            </div>
            <div class="col-md-4 col-sm-6">
                <h4 style="text-align: center">SecureZ Internet Security</h4>
				</br>
				<a href="securez internet security.html">
                    <img style="margin:auto" class="img-responsive img-portfolio img-hover" src="img/2.png" alt="">
                </a>
				</br>
				</br>
            </div>
            <div class="col-md-4 col-sm-6">
                <h4 style="text-align: center">SecureZ Anti Virus</h4>
				</br>
				<a href="securez anti virus.html">
                    <img style="margin:auto" class="img-responsive img-portfolio img-hover" src="img/3.png" alt="">
                </a>
				</br>
				</br>
            </div>
            <div class="col-md-4 col-sm-6">
                <h4 style="text-align: center">SecureZ Cleanup</h4>
				</br>
				<a href="securez cleanup.html">
                    <img style="margin:auto" class="img-responsive img-portfolio img-hover" src="img/5.png" alt="">
                </a>
				</br>
				</br>
            </div>
            <div class="col-md-4 col-sm-6">
                <h4 style="text-align: center">SecureZ Virus Scanner for Mac</h4>
				</br>
				<a href="securez virus scanner for mac.html">
                    <img style="margin:auto" class="img-responsive img-portfolio img-hover" src="img/6.png" alt="">
                </a>
				</br>
				</br>
            </div>
			<div class="col-md-4 col-sm-6">
                <h4 style="text-align: center">SecureZ Password Manager</h4>
				</br>
				<a href="securez password manager.html">
                    <img style="margin:auto" class="img-responsive img-portfolio img-hover" src="img/7.png" alt="">
                </a>
				</br>
				</br>
            </div>
        </div>
        <!-- /.row -->
        
        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>

</body>

</html>
