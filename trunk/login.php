<!DOCTYPE html>
<html>
  <head>
    <title>SecureZ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-bg">
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-12">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1 style="color:white">SecureZ</h1>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

	<div class="page-content container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-wrapper">
			        <div class="box">
			            <div class="content-wrap">
							    <div class="tab-content">
									<div id="login" class="tab-pane active">
										<form action="checklogin.php" class="form-signin" method="post">
											<p style="font-size: 25px"><b>User Login</b></p>
											<input type="text" placeholder="Username" name="username" required="required" class="form-control" />
											<input type="password" placeholder="Password" name="password" required="required" class="form-control" />
											<button class="btn text-muted text-center btn-danger" type="submit">LOGIN</button>
										</form> 
									</div>
									
									<div id="admin" class="tab-pane">
										<form action="checkadminlogin.php" class="form-signin" method="post">
											<p style="font-size: 25px"><b>Admin Login</b></p>
											<input type="text" placeholder="Username" name="username" required="required" class="form-control" />
											<input type="password" placeholder="Password" name="password" required="required" class="form-control" />
											<button class="btn text-muted text-center btn-danger" type="submit">LOGIN</button>
										</form>
									</div>
									
									<div id="register" class="tab-pane">
										<form action="register.php" class="form-signin" method="post">
											<p style="font-size: 25px"><b>Register</b></p>
											<input type="text" placeholder="Username" name="username" required="required" class="form-control" />
											<input type="password" placeholder="Password" name="password" required="required" class="form-control" />
											<input type="email" placeholder="Email" name="email" required="required" class="form-control" />
											<br/>
											<button class="btn text-muted text-center btn-danger" type="submit">REGISTER</button>
										</form>
									</div>
								</div>
								<div class="text-center">
									<br/>
									<ul class="list-inline">
										<li><a class="text-muted" href="#login" data-toggle="tab">Login</a></li>
										<li><a class="text-muted" href="#admin" data-toggle="tab">Admin</a></li>
										<li><a class="text-muted" href="#register" data-toggle="tab">Register</a></li>
									</ul>
								</div>
							</div>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>