<!DOCTYPE html>
<html>
  <head>
    <title>SecureZ Admin Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	
	<style>
	table, th, td {
		border: 1px solid black;
	}
	</style>
  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.html">SecureZ Admin Page</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="profile.html">Profile</a></li>
	                          <li><a href="login.html">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li><a href="index.html"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                    <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                    <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                    <li class="current"><a href="tables.php"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                    <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li>
                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="signup.html">Signup</a></li>
                        </ul>
                    </li>
                </ul>
             </div>
		  </div>
		  <div class="col-md-10">

		  	<div class="row">
  				<div class="col-md-6">
  					<div class="content-box-large">
		  				<div class="panel-heading">
							<div class="panel-title">Admin Table</div>
						</div>
		  				<div class="panel-body">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
								<tr>
									<th>Id</th>
									<th>Name</th>
									<th>Password</th>
								</tr>
							
								<?php
									mysql_connect("localhost", "root","") or die(mysql_error()); //Connect to server
									mysql_select_db("securez") or die("Cannot connect to database"); //connect to database
									$query = mysql_query("Select * from admin"); // SQL Query
									while($row = mysql_fetch_array($query))
									{
										Print "<tr>";
										Print '<td align="center">'. $row['id'] . "</td>";
										Print '<td align="center">'. $row['username'] . "</td>";
										Print '<td align="center">'. $row['password'] . "</td>";
										Print "</tr>";
									}
								?>
							</table>
		  				</div>
		  			</div>
  				</div>
  			</div>

  			<div class="content-box-large">
  				<div class="panel-heading">
					<div class="panel-title">Product Table</div>
				</div>
  				<div class="panel-body">
  					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
						<tr>
							<th>Product ID</th>
							<th>Product Name</th>
							<th>Product Description</th>
							<th>Product Price</th>
							<th>Supplier ID</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
							
						<?php
							mysql_connect("localhost", "root","") or die(mysql_error()); //Connect to server
							mysql_select_db("securez") or die("Cannot connect to database"); //connect to database
							$query = mysql_query("Select * from product"); // SQL Query
							while($row = mysql_fetch_array($query))
							{
								Print "<tr>";
								Print '<td align="center">'. $row['id'] . "</td>";
								Print '<td>'. $row['product_name'] . "</td>";
								Print '<td>'. $row['product_description'] . "</td>";
								Print '<td align="center">'. $row['product_price'] . "</td>";
								Print '<td align="center">'. $row['supplierID'] . "</td>";
								Print '<td align="center"><a href="edit.php?id='. $row['id'] .'">Edit</a> </td>';
								Print '<td align="center"><a href="#" onclick="myFunction('.$row['id'].')">Delete</a> </td>';
								Print "</tr>";
							}
						?>
						
						<script>
						function myFunction(id)
						{
							var r=confirm("Are you sure you want to delete this record?");
							if (r==true)
							{
								window.location.assign("delete.php?id=" + id);
							}
						}
						</script>
					</table>
  				</div>
				
				<form action="add.php" method="POST">
					<p2>ADD NEW ITEM<br>
					Product Name : <input type="text" name="product_name"/><br/>
					Product Description : <input type="text" name="product_description"/><br/>
					Product Price : <input type="text" name="product_price"/><br/>
					Supplier ID : <input type="text" name="supplierID"/><br/>
					</p2>
					<input class="btn btn-warning btn-sm" type="submit" value="Add"/>
				</form>
  			</div>
		  </div>
		</div>
    </div>

    <footer>
         <div class="container">
         
            <div class="copy text-center">
               Copyright 2014 <a href='#'>Website</a>
            </div>
            
         </div>
      </footer>

      <link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- jQuery UI -->
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>

    <script src="vendors/datatables/dataTables.bootstrap.js"></script>

    <script src="js/custom.js"></script>
    <script src="js/tables.js"></script>
  </body>
</html>