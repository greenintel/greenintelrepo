<html>
	  <head>
    <title>SecureZ Admin Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	
	<style>
	table, th, td {
		border: 1px solid black;
	}
	</style>
  </head>
	<body>
		<div class="page-content">
		<div class="content-box-large">
  		<div class="panel-heading">
			<div class="panel-title">Edit Product Table</div>
		</div>
  		<div class="panel-body">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">
			<tr>
				<th>Product ID</th>
				<th>Product Name</th>
				<th>Product Description</th>
				<th>Product Price</th>
				<th>Supplier ID</th>
			</tr>
			<?php
				if(!empty($_GET['id']))
				{
					$id = $_GET['id'];
					$_SESSION['id'] = $id;
					$id_exists = true;
					mysql_connect("localhost", "root","") or die(mysql_error()); //Connect to server
					mysql_select_db("securez") or die("Cannot connect to database"); //connect to database
					$query = mysql_query("Select * from product Where id='$id'"); // SQL Query
					$count = mysql_num_rows($query);
					if($count > 0)
					{
						while($row = mysql_fetch_array($query))
						{
							Print "<tr>";
								
								Print '<td align="center">'. $row['id'] . "</td>";
								Print '<td>'. $row['product_name'] . "</td>";
								Print '<td>'. $row['product_description'] . "</td>";
								Print '<td align="center">'. $row['product_price'] . "</td>";
								Print '<td align="center">'. $row['supplierID'] . "</td>";
							Print "</tr>";
						}
					}
					else
					{
						$id_exists = false;
					}
				}
			?>
		</table>
		<br/>
		<?php
		if($id_exists)
		{
		Print '
		<form action="edit.php" method="POST">
			Change Product Name : <input type="text" name="product_name"/><br/>
			Change Product Description : <input type="text" name="product_description"/><br/>
			Change Product Price : <input type="text" name="product_price"/><br/>
			Change Supplier ID : <input type="text" name="supplierID"/><br/>
			<input type="submit" value="Update List"/>
		</form>
		';
		}
		else
		{
			Print '<h2 align="center">There is no data to be edited.</h2>';
		}
		?>
		</div>
		</div>
		</div>
		</div>
		</div>
	</body>
</html>

<?php
	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		mysql_connect("localhost", "root","") or die(mysql_error()); //Connect to server
		mysql_select_db("securez") or die("Cannot connect to database"); //Connect to database
		$product_name = mysql_real_escape_string($_POST['product_name']);
		$product_description = mysql_real_escape_string($_POST['product_description']);
		$product_price = mysql_real_escape_string($_POST['product_price']);
		$supplierID = mysql_real_escape_string($_POST['supplierID']);
		$id = $_SESSION['id'];

		
		mysql_query("UPDATE product SET product_name='$product_name', product_description='$product_description', product_price='$product_price', supplierID=$'supplierID' WHERE id='$id'") ;

		header("location: tables.php");
	}
?>